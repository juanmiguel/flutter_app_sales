import 'package:dio/dio.dart';
import 'package:flutter_app_sales/app/data/models/product_model.dart';

import '../../core/config/app_config.dart';

class ProductProvider {
  Future<List<ProductModel>> getProducts({
    required String token,
  }) async {
    /*
    final _dio = Dio();
    final _response = await _dio.get(
      kBaseUrl + "api/house/houses/1/6",
      options: Options(
        headers: {
          "auth": token,
        },
      ),
    );
   */
    // return (_response.data["result"] as List)
    //     .map((json) => HouseModel.fromJson(json))
    //     .toList();

    return (productos as List)
        .map((json) => ProductModel.fromJson(json))
        .toList();
  }
}
