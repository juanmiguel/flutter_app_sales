import 'package:flutter_app_sales/app/data/providers/auth_provider.dart';
import 'package:flutter_app_sales/app/data/providers/local/auth_storage_provider.dart';
import 'package:flutter_app_sales/app/data/providers/product_provider.dart';
import 'package:flutter_app_sales/app/data/providers/user_provider.dart';
import 'package:flutter_app_sales/app/data/repositories/auth_repository.dart';
import 'package:flutter_app_sales/app/data/repositories/local/auth_storage_repository.dart';
import 'package:flutter_app_sales/app/data/repositories/user_repository.dart';
import 'package:get/get.dart';

import '../../data/repositories/product_repository.dart';

class DependecyInjection {
  static void load() {
    //Providers
    Get.put<AuthProvider>(AuthProvider());
    Get.put<UserProvider>(UserProvider());
    Get.put<ProductProvider>(ProductProvider());

    //Local
    Get.put<AuthStorageProvider>(AuthStorageProvider());

    //Repositories
    Get.put<AuthRepository>(AuthRepository());
    Get.put<UserRepository>(UserRepository());
    Get.put<ProductRepository>(ProductRepository());

    //Local
    Get.put<AuthStorageRepository>(AuthStorageRepository());
  }
}
