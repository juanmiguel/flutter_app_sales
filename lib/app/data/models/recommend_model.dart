class RecommendModel {
  final int? id;
  final String? name;
  final String? image;
  final String? price;
  final String? favorite;

  RecommendModel({this.id, this.name, this.image, this.price, this.favorite});
}

final recommends = [
  RecommendModel(
      id: 0,
      name: "2022 Original Unlocked For S21 Ultra 5g",
      image:
          'https://sc04.alicdn.com/kf/H78d74392221b4e6dbd59d0493c8f02574.jpg',
      price: '\$.500.00',
      favorite: "4.5"),
  RecommendModel(
      id: 1,
      name: "Jacket Men",
      image:
          'https://sc04.alicdn.com/kf/H4f2b2fcfb0454b558aed9b1035bfe5deg.jpg',
      price: '\$120.00',
      favorite: "4.2"),
  RecommendModel(
      id: 2,
      name: "Turbocharger for Volvo S70",
      image:
          'https://sc04.alicdn.com/kf/Hfa221fbf2a8d4f188c2b3288dbd5705ft.png',
      price: '\$3520.00',
      favorite: "4.1"),
  RecommendModel(
      id: 3,
      name: "Electric Bike",
      image:
          'https://sc04.alicdn.com/kf/H0b545d96a01b42cabc6561b47ed85458c.jpg_Q55.jpg',
      price: '\$2888.00',
      favorite: "4.7"),
  RecommendModel(
      id: 4,
      name: "9 Pcs pots and pans kitchen aluminum",
      image:
          'https://sc04.alicdn.com/kf/H0961df5fce334a308d8e8f87a50f51ebE.jpg',
      price: '\$500.00',
      favorite: "4.3"),
];
