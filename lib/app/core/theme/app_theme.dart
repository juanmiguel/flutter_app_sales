import 'dart:ui';

class AppTheme {
  static const Color background = Color.fromRGBO(240, 241, 248, 1.0);
  static const Color white = Color.fromRGBO(240, 241, 248, 1.0);
  static const Color blueDark = Color(0xFF143656);
  static const Color blue = Color(0xFF13497B);
  static const Color blueOpacity = Color(0xFF225C92);
  static const Color blueLight = Color(0xFF497FAF);
  static const Color cyan = Color(0xFF33BFC8);
  static const Color searchColor = Color(0xFF719280);
  static const Color light = Color.fromRGBO(168, 174, 196, 1.0);
  static const Color orange = Color(0xFFFF6E4E);
  static const Color red = Color.fromARGB(255, 224, 50, 11);
  static const Color blueDark100 = Color(0xFF010035);
  static const Color labelColor = Color(0xFF8A8989);
  static const Color yellow = Color(0xFFffcb66);
}

const itemsBottonNavigation = {
  'assets/icons/find_home.svg',
  'assets/icons/heart.svg',
  'assets/icons/location.svg',
  'assets/icons/message.svg'
};
