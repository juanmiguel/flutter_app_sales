import 'package:dio/dio.dart';
import 'package:flutter_app_sales/app/data/models/auth_model.dart';

import '../../core/config/app_config.dart';

class AuthProvider {
  Future<AuthModel> auth({
    required String email,
    required String password,
  }) async {
    final _dio = Dio();
    final _response = await _dio.post(
      kBaseUrl + "api/user/auth",
      data: {
        "email": email,
        "password": password,
      },
    );
    return AuthModel.fromJson(_response.data);
  }
}
