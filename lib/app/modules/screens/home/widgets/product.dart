import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/modules/screens/home/home_controller.dart';
import 'package:flutter_app_sales/app/modules/screens/home/widgets/product_item.dart';
import 'package:flutter_app_sales/app/routes/app_routes.dart';
import 'package:get/get.dart';

class Product extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) => SliverToBoxAdapter(
          child: Container(
        margin: EdgeInsets.only(right: 15, left: 15),
        child: Obx(() => Column(
            children: List.generate(
                _.products.length,
                (index) => ProductItem(
                    onTap: () {
                      Get.toNamed(AppRoutes.DETAIL,
                          arguments: _.products[index]);
                    },
                    data: _.products[index])))),
      )),
    );
  }
}
