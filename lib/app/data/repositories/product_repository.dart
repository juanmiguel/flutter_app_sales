import 'package:flutter_app_sales/app/data/models/product_model.dart';
import 'package:get/get.dart';
import '../providers/product_provider.dart';

class ProductRepository {
  final _apiProvider = Get.find<ProductProvider>();

  Future<List<ProductModel>> getProducts({
    required String token,
  }) =>
      _apiProvider.getProducts(token: token);
}
