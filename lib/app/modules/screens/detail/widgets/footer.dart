import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';
import 'package:flutter_app_sales/app/data/models/product_model.dart';
import 'package:flutter_app_sales/app/global/input_text.dart';
import 'package:flutter_app_sales/app/global/primary_button.dart';
import 'package:flutter_app_sales/app/modules/screens/detail/detail_controller.dart';
import 'package:get/get.dart';

class Footer extends StatefulWidget {
  @override
  FooterState createState() => FooterState();

  Footer({
    required this.product,
  });
  final ProductModel product;
}

class FooterState extends State<Footer> {
  final _amountController = TextEditingController();
  final _priceController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _amountController.dispose();
    _priceController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        height: 100.0,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              "${widget.product.price} USD",
              style: Theme.of(context).textTheme.headline6!.copyWith(
                  color: AppTheme.blueDark, fontWeight: FontWeight.w600),
            ),
            GestureDetector(
              onTap: () => _settingModalBottomSheet(context),
              child: Container(
                width: 170.0,
                height: 45.0,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: AppTheme.blueDark,
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Text(
                  "Add to cart",
                  style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white, fontWeight: FontWeight.w400),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _settingModalBottomSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (_) => GetBuilder<DetailController>(
        builder: (_) => Container(
          height: 600.0,
          padding: const EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            top: 30.0,
          ),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(20.0),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Buy",
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: AppTheme.blueDark,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              const SizedBox(height: 30.0),
              InputText(
                initialValue: "1.00",
                iconPrefix: Icons.app_registration_outlined,
                iconColor: AppTheme.light,
                border: InputBorder.none,
                keyboardType: TextInputType.number,
                labelText: "Amount",
                filled: false,
                enabledBorderColor: Colors.black26,
                focusedBorderColor: AppTheme.cyan,
                fontSize: 14.0,
                fontColor: Colors.black45,
                onChanged: _.onChangeAmount,
              ),
              const SizedBox(height: 20.0),
              InputText(
                initialValue: "${_.product.price}",
                iconPrefix: Icons.monetization_on,
                iconColor: AppTheme.light,
                border: InputBorder.none,
                keyboardType: TextInputType.number,
                labelText: "Price",
                filled: false,
                enabledBorderColor: Colors.black26,
                focusedBorderColor: AppTheme.cyan,
                readOnly: true,
                fontSize: 14.0,
                fontColor: Colors.black45,
              ),
              const SizedBox(height: 20.0),
              PrimaryButton(
                texto: "Save",
                onPressed: _.save,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
