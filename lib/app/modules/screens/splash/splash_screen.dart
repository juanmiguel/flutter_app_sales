import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';
import 'package:flutter_app_sales/app/modules/screens/splash/splash_controller.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      builder: (_) => Scaffold(
          backgroundColor: AppTheme.blueDark100,
          body: Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset("assets/icons/icon.png",
                    fit: BoxFit.contain, width: 50, height: 50),
                RichText(
                  text: TextSpan(
                    text: "Sales",
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Poppins',
                      color: AppTheme.white,
                    ),
                    children: [
                      TextSpan(
                        text: "24",
                        style: TextStyle(
                          fontSize: 26.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Poppins',
                          color: AppTheme.cyan,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20.0),
                Container(
                  width: 40.0,
                  child: LinearProgressIndicator(
                      backgroundColor: AppTheme.blueDark, color: AppTheme.cyan),
                ),
              ],
            ),
          )),
    );
  }
}
