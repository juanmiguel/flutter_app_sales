import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';
import 'package:flutter_app_sales/app/data/models/product_model.dart';
import 'custom_image.dart';

class ProductItem extends StatelessWidget {
  ProductItem({Key? key, required this.data, this.onTap}) : super(key: key);
  final ProductModel data;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: AppTheme.blueOpacity.withOpacity(0.1),
              spreadRadius: .5,
              blurRadius: 1,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                CustomImage(
                  "${data.image}",
                  width: 35,
                  height: 35,
                ),
                SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${data.name}",
                      style:
                          TextStyle(fontSize: 13, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      "${data.price}",
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "${data.description}",
              style: TextStyle(height: 1.5, color: AppTheme.blueDark),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Icon(
                  Icons.star,
                  size: 16,
                  color: AppTheme.yellow,
                ),
                Icon(
                  Icons.star,
                  size: 16,
                  color: AppTheme.yellow,
                ),
                Icon(
                  Icons.star,
                  size: 16,
                  color: AppTheme.yellow,
                ),
                Icon(
                  Icons.star,
                  size: 16,
                  color: AppTheme.yellow,
                ),
                Icon(
                  Icons.star_outline,
                  size: 16,
                  color: AppTheme.yellow,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
