import 'package:flutter_app_sales/app/data/models/product_model.dart';
import 'package:flutter_app_sales/app/global/snackbar.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class DetailController extends GetxController {
  ProductModel product = ProductModel();

  //Variables
  String _amount = "";

  @override
  void onInit() {
    product = Get.arguments as ProductModel;
    super.onInit();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  //funciones
  void onChangeAmount(String value) {
    _amount = value;
  }

  void save() async {
    Snackbar.show(
      title: "Successfull",
      message: "Added to cart",
    );
  }
}
