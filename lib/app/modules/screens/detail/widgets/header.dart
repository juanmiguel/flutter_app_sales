import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  Header({
    required this.path,
  });

  final String path;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 380.0,
      child: ConstrainedBox(
        child: Swiper(
          outer: false,
          itemBuilder: (context, index) {
            return Hero(
              tag: "key_$path",
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage("${path}"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            );
          },
          pagination: const SwiperPagination(
            margin: EdgeInsets.all(50.0),
          ),
          itemCount: 1,
        ),
        constraints: BoxConstraints.loose(
          const Size(200.0, 170.0),
        ),
      ),
    );
  }
}
