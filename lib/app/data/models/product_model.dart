class ProductModel {
  final int? id;
  final String? name;
  final String? description;
  final String? image;
  final String? price;
  final String? favorite;

  ProductModel(
      {this.id,
      this.name,
      this.description,
      this.image,
      this.price,
      this.favorite});

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        image: json["image"],
        price: json["price"],
        favorite: json["favorite"],
      );
}

final productos = [
  {
    "id": 1,
    "name": "scooters electric motorcycle",
    "description": "he point of using Lorem Ipsum is that it has",
    "image":
        'https://sc04.alicdn.com/kf/H498ca14b75f3429faf561d476bb65913Y.png',
    "price": '\$355.00',
    "favorite": "4.8"
  },
  {
    "id": 2,
    "name": "Pet Products No Coral Fleece",
    "description": " typesetting industry. Lorem Ipsum has been the industry's",
    "image":
        'https://sc04.alicdn.com/kf/Hfa9fda83bca24a1bab20c2cb5420a3dag.jpg',
    "price": '\$5.50',
    "favorite": "3.9"
  },
  {
    "id": 3,
    "name": "Men Printed Sleepwear in Pajama",
    "description":
        "printer took a galley of type and scrambled it to make a type",
    "image":
        'https://sc04.alicdn.com/kf/H1b4a24b90e2945cbaaba350db83ed449v.jpg',
    "price": '\$8.00',
    "favorite": "4.3"
  },
  {
    "id": 4,
    "name": "Roller Skates Shoes",
    "description":
        "It was popularised in the 1960s with the release of Letraset",
    "image":
        'https://sc04.alicdn.com/kf/Habf1c16ded614392ba468c4cf1f955d1C.jpg',
    "price": '\$16.50',
    "favorite": "2.8"
  },
  {
    "id": 5,
    "name": "Skateboard Electric Scooter Adult",
    "description":
        "It is a long established fact that a reader will be distracted",
    "image":
        'https://sc04.alicdn.com/kf/Hf78707e822d949e7a332b80bf9be95b90.jpg',
    "price": '\$599.00',
    "favorite": "4.8"
  },
  {
    "id": 6,
    "name": "scooters electric motorcycle",
    "description": "he point of using Lorem Ipsum is that it has",
    "image":
        'https://sc04.alicdn.com/kf/H498ca14b75f3429faf561d476bb65913Y.png',
    "price": '\$355.00',
    "favorite": "4.8"
  },
  {
    "id": 6,
    "name": "Tyros 5 76Key",
    "description": "Many desktop publishing packages and web page editors",
    "image":
        'https://sc04.alicdn.com/kf/U23594dad8b8d45538b455f3f318bee20t.jpg',
    "price": '\$455.00',
    "favorite": "4.8"
  }
];
