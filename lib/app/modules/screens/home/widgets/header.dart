import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../home_controller.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: AspectRatio(
        aspectRatio: 16 / 5,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: [
              _SliverAppBar(),
            ],
          ),
        ),
      ),
    );
  }
}

class _SliverAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) => Container(
        //margin: EdgeInsets.only(top: 60.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(
              () => RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: SvgPicture.asset(
                          "assets/icons/cart.svg",
                          width: 12.0,
                        ),
                      ),
                      alignment: PlaceholderAlignment.middle,
                    ),
                    TextSpan(
                      text: _.users.length > 0
                          ? "Welcome ${_.users[0].name} to SALES 24"
                          : "",
                      style: Theme.of(context).textTheme.button?.copyWith(
                          color: AppTheme.blueDark,
                          fontWeight: FontWeight.w600),
                    ),
                    WidgetSpan(
                      child: Icon(
                        Icons.favorite,
                        color: AppTheme.red,
                        size: 24.0,
                      ),
                      alignment: PlaceholderAlignment.middle,
                    ),
                  ],
                ),
              ),
            ),
            const CircleAvatar(
              foregroundImage: NetworkImage(
                  "https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg"),
            ),
          ],
        ),
      ),
    );
  }
}
