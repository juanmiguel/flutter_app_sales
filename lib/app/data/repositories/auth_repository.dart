import 'package:flutter_app_sales/app/data/models/auth_model.dart';
import 'package:flutter_app_sales/app/data/providers/auth_provider.dart';
import 'package:get/get.dart';

class AuthRepository {
  final _apiProvider = Get.find<AuthProvider>();

  Future<AuthModel> auth({
    required String email,
    required String password,
  }) =>
      _apiProvider.auth(email: email, password: password);
}
