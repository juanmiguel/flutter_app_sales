import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';
import 'package:flutter_app_sales/app/data/models/product_model.dart';
import 'package:flutter_svg/svg.dart';

class Content extends StatelessWidget {
  Content({
    required this.product,
  });

  final ProductModel product;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: double.infinity,
        height: MediaQuery.of(context).size.height * 0.60,
        padding: const EdgeInsets.only(
          top: 30.0,
          left: 20.0,
          right: 20.0,
          bottom: 20.0,
        ),
        decoration: BoxDecoration(
          color: AppTheme.white,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                          text: TextSpan(
                        children: [
                          WidgetSpan(
                            child: SvgPicture.asset(
                              "assets/icons/star.svg",
                              width: 20.0,
                              color: Colors.yellow,
                            ),
                            alignment: PlaceholderAlignment.middle,
                          ),
                          WidgetSpan(
                              child: Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              "${product.favorite}",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                      color: Colors.black54,
                                      fontWeight: FontWeight.w400),
                            ),
                          ))
                        ],
                      )),
                      const SizedBox(
                        height: 6.0,
                      ),
                      Text(
                        "${product.name}",
                        style: Theme.of(context).textTheme.headline5!.copyWith(
                            color: AppTheme.blueDark,
                            fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(
                        height: 6.0,
                      ),
                      Text(
                        "${product.description}",
                        style: Theme.of(context).textTheme.headline6!.copyWith(
                            color: AppTheme.blueDark,
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
                CircleAvatar(
                  foregroundImage: NetworkImage(product.image ?? ""),
                ),
              ],
            ),
            const SizedBox(
              height: 10.0,
            ),
            const SizedBox(
              height: 20.0,
            ),
            // ContentInformation(
            //   house: house,
            // ),
          ],
        ),
      ),
    );
  }

  // _utilitiesTags(BuildContext context, List<String> arrTags) {
  //   List<Widget> tags = [];

  //   arrTags.asMap().forEach((index, element) {
  //     tags.add(Text(
  //       element.replaceAll("'", ""),
  //       style: Theme.of(context)
  //           .textTheme
  //           .bodyText1!
  //           .copyWith(color: Colors.black54, fontWeight: FontWeight.w400),
  //     ));

  //     if (index < arrTags.length - 1) {
  //       tags.add(
  //         Padding(
  //           padding: const EdgeInsets.symmetric(horizontal: 8.0),
  //           child: CircleAvatar(
  //             backgroundColor: AppTheme.cyan.withOpacity(0.7),
  //             radius: 2.0,
  //           ),
  //         ),
  //       );
  //     }
  //   });
  //   return tags;
  // }

  // _infoRoom({
  //   required BuildContext context,
  //   required String pathImg,
  //   required int count,
  // }) {
  //   return Row(
  //     children: [
  //       SvgPicture.asset(
  //         pathImg,
  //         width: 22.0,
  //         color: Colors.cyan,
  //       ),
  //       Padding(
  //           padding: const EdgeInsets.symmetric(horizontal: 5.0),
  //           child: Text(
  //             "$count",
  //             style: Theme.of(context)
  //                 .textTheme
  //                 .headline6!
  //                 .copyWith(color: Colors.black54, fontWeight: FontWeight.w400),
  //           ))
  //     ],
  //   );
  // }

}
