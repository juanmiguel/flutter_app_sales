import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';
import 'package:flutter_app_sales/app/modules/screens/home/home_controller.dart';
import 'package:flutter_app_sales/app/modules/screens/home/widgets/Recommend.dart';
import 'package:flutter_app_sales/app/modules/screens/home/widgets/category.dart';
import 'package:flutter_app_sales/app/modules/screens/home/widgets/header.dart';
import 'package:flutter_app_sales/app/modules/screens/home/widgets/product.dart';
import 'package:flutter_app_sales/app/modules/screens/home/widgets/search.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) => Scaffold(
        backgroundColor: AppTheme.background,
        body: SafeArea(
          bottom: false,
          child: Stack(
            children: [
              Positioned.fill(
                child: CustomScrollView(
                  slivers: [
                    // Header(),
                    Search(),
                    Category(),
                    Recommend(),
                    Product(),
                  ],
                ),
              ),
              // ButtonNavigator()
            ],
          ),
        ),
      ),
    );
  }
}
