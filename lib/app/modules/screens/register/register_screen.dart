import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';
import 'package:flutter_app_sales/app/global/boton.dart';
import 'package:flutter_app_sales/app/global/input.dart';
import 'package:flutter_app_sales/app/global/labels.dart';
import 'package:flutter_app_sales/app/global/logo.dart';
import 'package:flutter_app_sales/app/modules/screens/register/register_controller.dart';
import 'package:flutter_app_sales/app/routes/app_routes.dart';

import 'package:get/get.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterController>(
        builder: (_) => Scaffold(
            backgroundColor: AppTheme.white,
            body: Container(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.95,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Logo(titulo: 'Registro'),
                      _Form(),
                      Labels(
                        ruta: 'login',
                        titulo: '¿Ya tienes una cuenta?',
                        subTitulo: '¡Ingresa ahora!',
                      ),
                    ],
                  ),
                ),
              ),
            )));
  }
}

class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final nameCtrl = TextEditingController();
  final emailCtrl = TextEditingController();
  final passCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 40),
      padding: EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        children: <Widget>[
          Input(
            icon: Icons.perm_identity,
            placeholder: 'Nombre',
            keyboardType: TextInputType.text,
            textController: nameCtrl,
          ),
          Input(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            keyboardType: TextInputType.emailAddress,
            textController: emailCtrl,
          ),
          Input(
            icon: Icons.lock_outline,
            placeholder: 'Contraseña',
            textController: passCtrl,
            isPassword: true,
          ),
          Boton(
            text: "REGISTRATE",
            color: AppTheme.blueOpacity,
            textColor: AppTheme.cyan,
            press: () {
              Get.toNamed(AppRoutes.LOGIN);
            },
          ),
        ],
      ),
    );
  }
}
