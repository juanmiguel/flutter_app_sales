import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';

class PrimaryButton extends StatelessWidget {
  PrimaryButton({
    required this.texto,
    required this.onPressed,
  });

  final void Function() onPressed;
  final String texto;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppTheme.blueDark100,
        borderRadius: BorderRadius.circular(29.0),
      ),
      child: MaterialButton(
        minWidth: double.infinity,
        height: 60.0,
        onPressed: onPressed,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          child: Text(
            texto,
            style: TextStyle(
              fontSize: 16.0,
              fontFamily: 'Poppins',
              color: AppTheme.white,
            ),
          ),
        ),
      ),
    );
  }
}
