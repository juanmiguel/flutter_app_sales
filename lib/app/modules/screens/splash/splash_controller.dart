import 'package:flutter_app_sales/app/routes/app_routes.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    _doLogin();
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  _doLogin() async {
    try {
      await Future.delayed(
          Duration(seconds: 3), () => Get.offNamed(AppRoutes.LOGIN));
    } catch (e) {
      print(e);
    }
  }
}
