import 'package:dio/dio.dart';
import 'package:flutter_app_sales/app/data/models/auth_model.dart';
import 'package:flutter_app_sales/app/data/models/product_model.dart';
import 'package:flutter_app_sales/app/data/models/user_model.dart';
import 'package:flutter_app_sales/app/data/repositories/local/auth_storage_repository.dart';
import 'package:flutter_app_sales/app/data/repositories/product_repository.dart';
import 'package:flutter_app_sales/app/data/repositories/user_repository.dart';
import 'package:flutter_app_sales/app/global/load_spinner.dart';
import 'package:flutter_app_sales/app/global/snackbar.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  //Instancias
  final _userRepository = Get.find<UserRepository>();
  final _authStorageRepository = Get.find<AuthStorageRepository>();
  final _productRepository = Get.find<ProductRepository>();

  AuthModel authModel = AuthModel();

  //Variables
  RxInt isSelectedIndex = RxInt(0);

  RxList<ProductModel> products = RxList<ProductModel>([]);
  RxList<UserModel> users = RxList<UserModel>([]);

  @override
  void onInit() {
    // TODO: implement onInit
    _loadSession();
    super.onInit();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  selectedIndex(int index) {
    isSelectedIndex.value = index;
  }

  _loadSession() async {
    try {
      authModel = await _authStorageRepository.getSession(key: "auth");
      _loadInformation();
      _loadProducts();
    } catch (e) {
      Snackbar.show(
        title: "Error Auth",
        message: e.toString(),
      );
    }
  }

  _loadProducts() async {
    try {
      products.value = await _productRepository.getProducts(
        token: "${authModel.requestToken}",
      );

      print('Cargó los productos');
    } on DioError catch (e) {
      LoadSpinner.hide();
      Snackbar.show(
        title: "Error load products",
        message: e.response?.data['message'],
        type: 1,
      );
    }
  }

  _loadInformation() async {
    try {
      users.value = await _userRepository.getUser(
        token: "${authModel.requestToken}",
        idUser: authModel.idUser ?? 0,
      );
    } on DioError catch (e) {
      LoadSpinner.hide();
      Snackbar.show(
        title: "Error load products",
        message: e.response?.data['message'],
        type: 1,
      );
    }
  }
}
