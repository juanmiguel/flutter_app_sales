import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/modules/screens/home/home_controller.dart';
import 'package:flutter_app_sales/app/modules/screens/home/widgets/feature_item.dart';
import 'package:flutter_app_sales/app/routes/app_routes.dart';
import 'package:get/get.dart';

class Recommend extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) => SliverToBoxAdapter(
          child: Obx(() => CarouselSlider(
              options: CarouselOptions(
                height: 290,
                enlargeCenterPage: true,
                disableCenter: true,
                viewportFraction: .75,
              ),
              items: List.generate(
                  _.products.length,
                  (index) => FeatureItem(
                      onTap: () {
                        Get.toNamed(AppRoutes.DETAIL,
                            arguments: _.products[index]);
                      },
                      data: _.products[index]))))),
    );
  }
}
