import 'package:flutter_app_sales/app/modules/screens/detail/detail_screen.dart';
import 'package:flutter_app_sales/app/modules/screens/home/home_binding.dart';
import 'package:flutter_app_sales/app/modules/screens/home/home_screen.dart';
import 'package:flutter_app_sales/app/modules/screens/login/login_biding.dart';
import 'package:flutter_app_sales/app/modules/screens/login/login_screen.dart';
import 'package:flutter_app_sales/app/modules/screens/register/register_binding.dart';
import 'package:flutter_app_sales/app/modules/screens/register/register_screen.dart';
import 'package:flutter_app_sales/app/modules/screens/splash/splash_binding.dart';
import 'package:flutter_app_sales/app/modules/screens/splash/splash_screen.dart';
import 'package:get/get.dart';

import '../modules/screens/detail/detail_binding.dart';
import 'app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
        name: AppRoutes.SPLASH,
        page: () => const SplashScreen(),
        binding: SplashBinding()),
    GetPage(
        name: AppRoutes.LOGIN,
        page: () => const LoginScreen(),
        binding: LoginBinding()),
    GetPage(
        name: AppRoutes.REGISTER,
        page: () => const RegisterScreen(),
        binding: RegisterBinding()),
    GetPage(
      name: AppRoutes.HOME,
      page: () => HomeScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: AppRoutes.DETAIL,
      page: () => DetailScreen(),
      binding: DetailBinding(),
    ),
  ];
}
