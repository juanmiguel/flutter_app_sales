import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';
import 'package:flutter_app_sales/app/modules/screens/detail/detail_controller.dart';
import 'package:flutter_app_sales/app/modules/screens/detail/widgets/content.dart';
import 'package:flutter_app_sales/app/modules/screens/detail/widgets/footer.dart';
import 'package:flutter_app_sales/app/modules/screens/detail/widgets/header.dart';
import 'package:get/get.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetailController>(
      builder: (_) => Scaffold(
        //appBar: AppBar(),
        backgroundColor: AppTheme.background,
        body: Stack(
          children: [
            Header(path: _.product.image ?? ""),
            Content(product: _.product),
            Footer(product: _.product),
          ],
        ),
      ),
    );
  }
}
