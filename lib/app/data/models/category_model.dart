import 'package:meta/meta.dart' show required;

class CategoryModel {
  final int? id;
  final String? name;
  final String? path;

  CategoryModel({
    @required this.id,
    @required this.name,
    @required this.path,
  });
}

final categories = [
  CategoryModel(
    id: 0,
    name: 'Electronic',
    path: 'assets/icons/electronic.svg',
  ),
  CategoryModel(
    id: 1,
    name: 'Appareal',
    path: 'assets/icons/apparel.svg',
  ),
  CategoryModel(
    id: 2,
    name: 'Vehicle',
    path: 'assets/icons/vehicle.svg',
  ),
  CategoryModel(
    id: 3,
    name: 'Sport',
    path: 'assets/icons/sport.svg',
  ),
  CategoryModel(
    id: 4,
    name: 'Machinery',
    path: 'assets/icons/machinery.svg',
  ),
  CategoryModel(
    id: 5,
    name: 'Home',
    path: 'assets/icons/home.svg',
  ),
  CategoryModel(
    id: 6,
    name: 'Beauty',
    path: 'assets/icons/beauty.svg',
  ),
];
