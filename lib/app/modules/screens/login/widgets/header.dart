import 'package:flutter/material.dart';
import 'package:flutter_app_sales/app/core/theme/app_theme.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 16.0 / 9.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset("assets/icons/icon.png",
              fit: BoxFit.contain, width: 50, height: 50),
          RichText(
            text: TextSpan(
              text: "Sales",
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
                fontFamily: 'Poppins',
                color: AppTheme.white,
              ),
              children: [
                TextSpan(
                  text: "24",
                  style: TextStyle(
                    fontSize: 26.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins',
                    color: AppTheme.cyan,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
